package org.tensorflow.demo.env;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Outline;
import android.graphics.Path;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewOutlineProvider;

import org.tensorflow.demo.classifier.Classifier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by maksim on 15.04.17.
 */

public final class Utils {

    /**
     * Convert a dp float value to pixels
     *
     * @param dp float value in dps to convert
     * @return DP value converted to pixels
     */
    public static int dp2px(Context context, float dp) {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
        return Math.round(px);
    }

    /**
     * A helper class for providing a shadow on sheets
     */
    @TargetApi(21)
    public static class ShadowOutline extends ViewOutlineProvider {

        int width;
        int height;

        public ShadowOutline(int width, int height) {
            this.width = width;
            this.height = height;
        }

        @Override
        public void getOutline(View view, Outline outline) {
            outline.setRect(0, 0, width, height);
        }

    }

    public static HashMap<String, String> loadTxtFile(Context context, String nameFile) {
        HashMap<String, String> map = new HashMap<>();
        String word;
        try {
            AssetManager assetManager = context.getAssets();
            InputStreamReader istream = new InputStreamReader(assetManager.open(nameFile));
            BufferedReader in = new BufferedReader(istream);

            while ((word = in.readLine()) != null) {
                String[] words = word.split("-");
                String put = map.put(words[0], words[1]);
            }
            in.close();
        } catch (IOException error){

        }
        return map;
    }

    public static void createDirectoryAndSaveFile(Bitmap imageToSave,
                                           Classifier.Recognition item) {
        // Mask: "category" - "percentage" - "date" / "time" .jpg
        //Example: lucanus-capreolus-82-date/time.jpg

        File direct = new File(Environment.getExternalStorageDirectory() + "/customtensorflow");
        if (!direct.exists()) {
            File wallpaperDirectory = new File("/sdcard/customtensorflow/");
            wallpaperDirectory.mkdirs();
        }

        Calendar c = Calendar.getInstance();
        StringBuilder subdirectBuilder = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        subdirectBuilder.append(item.getTitle());
        subdirectBuilder.append("-");
        subdirectBuilder.append(String.format("%.2f", item.getConfidence()*100));
        subdirectBuilder.append("-");
        subdirectBuilder.append(c.get(Calendar.MONTH));
        subdirectBuilder.append(".");
        subdirectBuilder.append(c.get(Calendar.DATE));
        subdirectBuilder.append(".");
        subdirectBuilder.append(c.get(Calendar.YEAR));
        String subdirectPath = subdirectBuilder.toString();
        File subdirect = new File(direct+"/"+subdirectPath);
        if (!subdirect.exists()) {
            File wallpaperDirectory = new File("/sdcard/customtensorflow/"+subdirectPath+"/");
            wallpaperDirectory.mkdirs();
        }

        File file = new File(subdirect, sdf.format(new Date()) + ".jpg");

        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
