package org.tensorflow.demo.ui.imageanalysis;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import org.tensorflow.demo.R;
import org.tensorflow.demo.ui.settings.SettingsActivity;

public class ImageAnalysisActivity extends AppCompatActivity {

    private static final String ARG_SELECTED_IMAGE_URI = "selected_image_uri";

    public static void start(@NonNull Activity activity, @NonNull Uri selectedImageUri) {
        Intent intent = new Intent(activity, ImageAnalysisActivity.class);
        intent.putExtra(ARG_SELECTED_IMAGE_URI, selectedImageUri);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.analysis_image_activity);
        setupActionBar();
        if (savedInstanceState == null) {
            setFragment();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.image_analysis_activity_option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menu_item_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void setFragment() {
        Uri selectedImageUri = getIntent().getParcelableExtra(ARG_SELECTED_IMAGE_URI);
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, ImageAnalysisFragment.newInstance(selectedImageUri))
                .commit();
    }
}