package org.tensorflow.demo.ui.imageanalysis;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import org.tensorflow.demo.classifier.Classifier;
import org.tensorflow.demo.R;
import org.tensorflow.demo.classifier.TensorFlowImageClassifier;
import org.tensorflow.demo.env.ImageUtils;
import org.tensorflow.demo.env.Logger;
import org.tensorflow.demo.env.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static org.tensorflow.demo.ui.main.MainActivity.AL_LABEL_FILE;
import static org.tensorflow.demo.ui.main.MainActivity.IMAGE_MEAN;
import static org.tensorflow.demo.ui.main.MainActivity.IMAGE_STD;
import static org.tensorflow.demo.ui.main.MainActivity.INPUT_NAME;
import static org.tensorflow.demo.ui.main.MainActivity.INPUT_SIZE;
import static org.tensorflow.demo.ui.main.MainActivity.LABEL_FILE;
import static org.tensorflow.demo.ui.main.MainActivity.MODEL_FILE;
import static org.tensorflow.demo.ui.main.MainActivity.OUTPUT_NAME;

/**
 * Created by maksim on 18.04.17.
 */

public class ImageAnalysisFragment extends Fragment {
    private static final Logger LOGGER = new Logger();
    private static final String ARG_SELECTED_IMAGE_URI = "selected_image_uri";
    private Uri selectedImageUri;

    private final CompositeSubscription compositeSubscription = new CompositeSubscription();
    private Classifier classifier;
    private HashMap<String, String> alter_names;
    private AnalysisPictureTask task;
    private SharedPreferences prefs;

    // Views
    private ImageView image;
    private TextView resultsTextView;

    public static
    @NonNull
    ImageAnalysisFragment newInstance(@NonNull Uri selectedImageUri) {
        Bundle args = new Bundle();
        args.putParcelable(ARG_SELECTED_IMAGE_URI, selectedImageUri);

        ImageAnalysisFragment fragment = new ImageAnalysisFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        classifier = TensorFlowImageClassifier.create(getActivity().getAssets(),
                MODEL_FILE,
                LABEL_FILE,
                INPUT_SIZE,
                IMAGE_MEAN,
                IMAGE_STD,
                INPUT_NAME,
                OUTPUT_NAME);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.analysis_image_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        image = (ImageView) view.findViewById(R.id.image);
        resultsTextView = (TextView) view.findViewById(R.id.results_textview);

        selectedImageUri = getArguments().getParcelable(ARG_SELECTED_IMAGE_URI);
        alter_names = Utils.loadTxtFile(getActivity(), AL_LABEL_FILE);

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        Glide.with(this)
                .load(selectedImageUri)
                .into(image);
    }

    @Override
    public void onResume() {
        super.onResume();
        task = new AnalysisPictureTask();
        task.execute();
    }

    @Override
    public void onPause() {
        task.cancel(true);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        compositeSubscription.clear();
        super.onDestroy();
    }

    private class AnalysisPictureTask extends AsyncTask<Void, String, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            resultsTextView.setText(values[0]);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            publishProgress("Start creating assumptions. Image upload");
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImageUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (bitmap == null) return null;
            publishProgress("Preparing the image");
            Bitmap croppedBitmap = Bitmap.createBitmap(INPUT_SIZE, INPUT_SIZE, Bitmap.Config.ARGB_8888);
            ImageUtils.drawResizedBitmap(bitmap, croppedBitmap, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            publishProgress("Image recognition");
            final List<Classifier.Recognition> results = classifier.recognizeImage(croppedBitmap);
            if (results.isEmpty()) {
                publishProgress("Could find a match!");
            } else {
                publishProgress("Creating a report");
                StringBuilder final_string = new StringBuilder();
                for (int i = 0; i < results.size(); i++) {
                    final_string.append(String.format("%.2f",
                            results.get(i).getConfidence() * 100));
                    final_string.append("%");
                    final_string.append(" ");
                    if(prefs.getString("class_name_type","0").equals("0"))
                        final_string.append(results.get(i).getTitle());
                    else
                        final_string.append(alter_names.get(results.get(i).getTitle()));
                    if (i != results.size() - 1)
                        final_string.append("\n");
                    publishProgress("Saving an image to a special folder");
                    Utils.createDirectoryAndSaveFile(bitmap, results.get(i));
                }
                publishProgress(final_string.toString());
            }
            return null;
        }
    }
}